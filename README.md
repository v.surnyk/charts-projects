# kubernetes-deployments

In order to create the App with configured service, deployment, ingress in k8s: 

1. Install kubectl utility
2. Use the following commands to deploy the App with configured service, deployment, ingress in required namespace:

```
kubectl apply -f deployment.yaml -n app
kubectl apply -f service.yaml -n app
kubectl apply -f ingress.yaml -n app
```

Or install helm utility to deploy the App via Helm Chart:

```
helm install test-01 Chart-task-01/ -n app
```
Where `test-01` is name of app  and `-n app` is name of namespace (if namespace does not exist use option `--create-namespace` )

3. install nginx-ingress controller :

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install ingress-nginx ingress-nginx/ingress-nginx
```
4. DNS name http://v.int.itoutposts.com/ was provided for LoadBalancer IP 34.118.50.130

5.  install the cert-manager to update SSL certifiacte automatically

```
helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager --create-namespace \
  --version v1.10.0 \
  --set installCRDs=true

```

6. create letsencrypt-production issuer to provide SSL sertificate for your DNS name

```
kubectl apply -f ClusterIssuer.yaml -n app

```


